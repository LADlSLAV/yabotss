#!/bin/bash

homedir=/home/user
dark_profile=dark
light_profile=sol_light

servicename=org.freedesktop.portal.Desktop
path=/org/freedesktop/portal/desktop
method=org.freedesktop.portal.Settings.Read
args="org.freedesktop.appearance color-scheme"
colorscheme=$(qdbus $servicename $path $method $args)
if test $colorscheme -eq 1
then
    profile=$dark_profile
else
    profile=$light_profile
fi

konsoles=($(pidof konsole))
dolphins=($(pidof dolphin))
kates=($(pidof kate))

services=("${konsoles[*]/#/org.kde.konsole-} ${dolphins[*]/#/org.kde.dolphin-} ${kates[*]/#/org.kde.kate-}")

set_profile_method="org.kde.konsole.Session.setProfile"

for servicename in $services
do
    sessions=$(qdbus $servicename | grep /Sessions/)
    for session in $sessions
    do
        qdbus $servicename $sessions $set_profile_method $profile
    done
done

kwriteconfig5 --file $homedir/.config/konsolerc --group "Desktop Entry" --key "DefaultProfile" $profile.profile
