#!/bin/bash

homedir=/home/user

config_paths=(  "$homedir/.config/VSCodium/User/settings.json"
                "$homedir/.config/Code - OSS/User/settings.json"
                "$homedir/.config/Code/User/settings.json"
            )

servicename=org.freedesktop.portal.Desktop
path=/org/freedesktop/portal/desktop
method=org.freedesktop.portal.Settings.Read
args="org.freedesktop.appearance color-scheme"
colorscheme=$(qdbus $servicename $path $method $args)
if test $colorscheme -eq 1
then
    preffere_theme="\"workbench.preferredDarkColorTheme\"";
else
    preffere_theme="\"workbench.preferredLightColorTheme\"";
fi

tmp=$(mktemp)
for config_path in "${config_paths[@]}"
do
    echo "$config_path"
    if [[ -f "$config_path" ]]; then
        echo "exists."
        jq ".\"workbench.colorTheme\" = .$preffere_theme" $config_path > $tmp && mv $tmp $config_path
    fi
done
