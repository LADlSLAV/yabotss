# Yet another bunch of theme switching scripts

## Dark/light theme-following scripts for KDE konsole, okular and vscodium

### What it does

Based on active system theme:

- set_theme_konsole.sh switches to correct konsole profile in all konsoles as well as terminal panels in dolphins and kates

- set_theme_vscodium.sh switches to correct preferred vscode theme

- set_theme_okular.sh enables/disables okular's accessability color mode (good idea si to add actions for different color modes to the toolbar for easy switching)

- darkman example scrips for KDE (does the same thing without the need to check system theme + actually change wallpaper and system look (for some reason applying lookandfeeltool once sometimes does not apply dark theme or light theme in some gtk apps - workaround is to spam lookandfeeltool with same theme))

### Usage

Run the scripts after you change the theme (you can find a way to automate that on your own)

In every script you have to set correct `homedir` path.For kate you need to have dark and light profiles created and set `dark_profile` and `light_profile` variables according to profile names in konsole's profile manager.

If you use or want to use https://gitlab.com/WhyNotHugo/darkman/ for the scheduling and theme switching, follow it's setup instructions and just copy `dark-mode.d/` and `light-mode.d/` folders into one of your `$XDG_DATA_DIRS`.
