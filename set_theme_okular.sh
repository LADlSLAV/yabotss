#!/bin/bash

homedir=/home/user

servicename=org.freedesktop.portal.Desktop
path=/org/freedesktop/portal/desktop
method=org.freedesktop.portal.Settings.Read
args="org.freedesktop.appearance color-scheme"
colorscheme=$(qdbus $servicename $path $method $args)
if test $colorscheme -eq 1
then
    darkmode=true
else
    darkmode=false
fi

okulars=($(pidof okular))
okulars=("${okulars[*]/#/org.kde.okular-}")

set_colors_method="org.kde.okular.slotSetChangeColors"
for okular in $okulars
do
    qdbus $okular /okular $set_colors_method $darkmode
done

kwriteconfig5 --file $homedir/.config/okularpartrc --group "Document" --key "ChangeColors" --type "bool" "$darkmode"
