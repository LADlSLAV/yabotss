#!/bin/bash

homedir=/home/user

darkmode=false

okulars=($(pidof okular))
okulars=("${okulars[*]/#/org.kde.okular-}")

set_colors_method="org.kde.okular.slotSetChangeColors"
for okular in $okulars
do
    qdbus $okular /okular $set_colors_method $darkmode
done

kwriteconfig5 --file $homedir/.config/okularpartrc --group "Document" --key "ChangeColors" --type "bool" "$darkmode"
