#!/bin/bash

homedir=/home/user

config_paths=(  "$homedir/.config/VSCodium/User/settings.json"
                "$homedir/.config/Code - OSS/User/settings.json"
                "$homedir/.config/Code/User/settings.json"
            )

preffere_theme="\"workbench.preferredDarkColorTheme\"";

tmp=$(mktemp)
for config_path in "${config_paths[@]}"
do
    echo "$config_path"
    if [[ -f "$config_path" ]]; then
        echo "exists."
        jq ".\"workbench.colorTheme\" = .$preffere_theme" $config_path > $tmp && mv $tmp $config_path
    fi
done
