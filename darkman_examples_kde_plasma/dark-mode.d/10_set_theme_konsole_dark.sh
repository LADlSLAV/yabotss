#!/bin/bash

homedir=/home/user
dark_profile=dark
light_profile=sol_light

profile=$dark_profile

konsoles=($(pidof konsole))
dolphins=($(pidof dolphin))
kates=($(pidof kate))

services=("${konsoles[*]/#/org.kde.konsole-} ${dolphins[*]/#/org.kde.dolphin-} ${kates[*]/#/org.kde.kate-}")

set_profile_method="org.kde.konsole.Session.setProfile"

for servicename in $services
do
    sessions=$(qdbus $servicename | grep /Sessions/)
    for session in $sessions
    do
        qdbus $servicename $sessions $set_profile_method $profile
    done
done

kwriteconfig5 --file $homedir/.config/konsolerc --group "Desktop Entry" --key "DefaultProfile" $profile.profile
